# Scrumify Challenge (BETA)

This repository contains the material and gives useful hints about the scrumify challenge from Journey to Cloud - New Joiners KIT

## Jenkins/SonarQube VM Setup

These VM's will be required to create a CI/CD pipeline later for the scrumify application.

[Jenkins](/setup_documentation/jenkins_machine_setup.md)

[SonarQube](/setup_documentation/sonarqube_machine_setup.md)

## Cluster Setup

[Cluster GKE](/setup_documentation/cluster_setup.md)

## Scrumify Deploy

[Scrumify](/setup_documentation/scrumify_setup.md)

## Apigee 

[Apigee](/setup_documentation/apigee_setup.md)

