FROM adoptopenjdk/openjdk11:jdk-11.0.6_10-alpine-slim
#FROM maven:3.6.2-jdk-11

ENV SCRUMIFY_HOME=/opt/scrumify

VOLUME $SCRUMIFY_HOME
WORKDIR $SCRUMIFY_HOME

COPY target/scrumify-*.jar scrumify.jar

EXPOSE 8080

ENTRYPOINT java -jar -Dspring.profiles.active=cloud scrumify.jar