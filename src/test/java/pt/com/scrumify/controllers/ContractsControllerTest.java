package pt.com.scrumify.controllers;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.isA;
import static org.hamcrest.Matchers.emptyOrNullString;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import pt.com.scrumify.annotations.SmokeTest;
import pt.com.scrumify.annotations.UnitTest;
import pt.com.scrumify.configuration.MockScrumifyUser;
import pt.com.scrumify.database.services.ContractService;
import pt.com.scrumify.entities.ContractView;
import pt.com.scrumify.helpers.ConstantsHelper;

@AutoConfigureMockMvc
@DisplayName("Test of contracts controller")
@SpringBootTest
public class ContractsControllerTest {
   @Autowired
   private MockMvc mvc;
   @Autowired
   ContractService service;

   @DisplayName("Test of contracts list mapping (get) by not allowed user (anonymous)")
   @Test
   @SmokeTest
   @MockScrumifyUser(username = "anonymous")  
   public void listByNotAllowedUser() throws Exception {
      mvc.perform(get(ConstantsHelper.MAPPING_CONTRACTS))
         .andExpect(status().isForbidden())
         ;
   }

   @DisplayName("Test of contracts list mapping (get) by allowed user (projectdirector)")
   @Test
   @SmokeTest
   @MockScrumifyUser(username = "projectdirector")
   public void listByAllowedUser() throws Exception {
      int EXPECTED_SIZE = 2;
      
      mvc.perform(get(ConstantsHelper.MAPPING_CONTRACTS))
         .andExpect(status().isOk())
         .andExpect(view().name(ConstantsHelper.VIEW_CONTRACTS_READ))
         .andExpect(model().attribute(ConstantsHelper.VIEW_ATTRIBUTE_CONTRACTS, hasSize(EXPECTED_SIZE)))
         ;
   }
   
   @Test
   @UnitTest
   @MockScrumifyUser(username = "projectdirector")  
   public void createByAllowedUser() throws Exception {
      mvc.perform(get(ConstantsHelper.MAPPING_CONTRACTS_CREATE))
         .andExpect(status().isOk())
         .andExpect(view().name(ConstantsHelper.VIEW_CONTRACTS_SAVE))
         .andExpect(model().attribute(ConstantsHelper.VIEW_ATTRIBUTE_CONTRACT, isA(ContractView.class)))
         .andExpect(model().attribute(ConstantsHelper.VIEW_ATTRIBUTE_CONTRACT, hasProperty("id", is(0))))
         .andExpect(model().attribute(ConstantsHelper.VIEW_ATTRIBUTE_CONTRACT, hasProperty("description", is(emptyOrNullString()))))
         .andExpect(model().attribute(ConstantsHelper.VIEW_ATTRIBUTE_CONTRACT, hasProperty("name", is(emptyOrNullString()))))
         ;
   }
}