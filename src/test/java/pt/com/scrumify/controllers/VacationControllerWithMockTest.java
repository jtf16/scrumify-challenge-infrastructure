package pt.com.scrumify.controllers;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.csrf;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import pt.com.scrumify.annotations.UnitTest;
import pt.com.scrumify.configuration.MockScrumifyUser;
import pt.com.scrumify.database.entities.Vacation;
import pt.com.scrumify.database.services.VacationService;
import pt.com.scrumify.helpers.ConstantsHelper;

@AutoConfigureMockMvc
@DisplayName("Test of vacations with mock controller")
@SpringBootTest
public class VacationControllerWithMockTest {
   @Autowired
   private MockMvc mvc;
   
   @MockBean
   VacationService mock;
   
   @Test
   @UnitTest
   @MockScrumifyUser(username = "projectdirector")  
   public void saveByAllowedUser() throws Exception {
      String content = "id=0&";
   
      when(mock.save(any(Vacation.class))).thenReturn(new Vacation());
       
      mvc.perform(post(ConstantsHelper.MAPPING_VACATIONS_SAVE)
             .content(content)
             .with(csrf())
             .contentType(MediaType.APPLICATION_FORM_URLENCODED)
             .accept(MediaType.APPLICATION_FORM_URLENCODED))
      .andExpect(status().isOk())
      .andReturn()
      ;
   }

}