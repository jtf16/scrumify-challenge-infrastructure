package pt.com.scrumify.controllers;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.isA;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.emptyOrNullString;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasProperty;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;

import pt.com.scrumify.annotations.UnitTest;
import pt.com.scrumify.configuration.MockScrumifyUser;
import pt.com.scrumify.database.services.SubAreaService;
import pt.com.scrumify.entities.SubAreaView;
import pt.com.scrumify.helpers.ConstantsHelper;

@AutoConfigureMockMvc
@DisplayName("Test of subareas controller")
@SpringBootTest
public class SubAreasControllerTest {
   @Autowired
   private MockMvc mvc;

   @Autowired
   SubAreaService service;

   @DisplayName("Test of subareas create mapping (get) by not allowed user (projectdirector)")
   @Test
   @UnitTest
   @MockScrumifyUser(username = "projectdirector")  
   public void createNotByAllowedUser() throws Exception {
      mvc.perform(get(ConstantsHelper.MAPPING_SUBAREAS_CREATE))
         .andExpect(status().isForbidden())
         ;
   }
   
   @DisplayName("Test of subareas create mapping (get) by allowed user (admin)")
   @Test
   @UnitTest
   @MockScrumifyUser(username = "admin")  
   public void createByAllowedUser() throws Exception {
      mvc.perform(get(ConstantsHelper.MAPPING_SUBAREAS_CREATE))
         .andExpect(status().isOk())
         .andExpect(view().name(ConstantsHelper.VIEW_SUBAREAS_CREATE))
         .andExpect(model().attribute(ConstantsHelper.VIEW_ATTRIBUTE_SUBAREA, isA(SubAreaView.class)))
         .andExpect(model().attribute(ConstantsHelper.VIEW_ATTRIBUTE_SUBAREA, hasProperty("id", is(0))))
         .andExpect(model().attribute(ConstantsHelper.VIEW_ATTRIBUTE_SUBAREA, hasProperty("abbreviation", is(emptyOrNullString()))))
         .andExpect(model().attribute(ConstantsHelper.VIEW_ATTRIBUTE_SUBAREA, hasProperty("name", is(emptyOrNullString()))))
         .andExpect(model().attribute(ConstantsHelper.VIEW_ATTRIBUTE_SUBAREA, hasProperty("area", hasProperty("id", is(0)))))
         ;
   }

   @DisplayName("Test of subareas list mapping (get) by not allowed user (developer1)")
   @Test
   @UnitTest
   @MockScrumifyUser(username = "developer1")  
   public void listByNotAllowedUser() throws Exception {
      mvc.perform(get(ConstantsHelper.MAPPING_SUBAREAS))
         .andExpect(status().isForbidden())
         ;
   }

   @DisplayName("Test of areas list mapping (get) by allowed user (projectlead2)")
   @Test
   @UnitTest
   @MockScrumifyUser(username = "projectlead2")  
   public void listByAllowedUser() throws Exception {
      int EXPECTED_SIZE = 10;
      
      mvc.perform(get(ConstantsHelper.MAPPING_SUBAREAS))
         .andExpect(status().isOk())
         .andExpect(view().name(ConstantsHelper.VIEW_SUBAREAS_READ))
         .andExpect(model().attribute(ConstantsHelper.VIEW_ATTRIBUTE_SUBAREAS, hasSize(EXPECTED_SIZE)))
         .andExpect(model().attribute(ConstantsHelper.VIEW_ATTRIBUTE_SUBAREAS, hasItem(allOf(hasProperty("id", is(1)),
                                                                                             hasProperty("area", hasProperty("id", is(1))),
                                                                                             hasProperty("abbreviation", is("NIGC")),
                                                                                             hasProperty("name", is("Núcleo de Identificação e Gestão de Contribuintes"))))))
         .andExpect(model().attribute(ConstantsHelper.VIEW_ATTRIBUTE_SUBAREAS, hasItem(allOf(hasProperty("id", is(2)),
                                                                                             hasProperty("area", hasProperty("id", is(1))),
                                                                                             hasProperty("abbreviation", is("NSAI")),
                                                                                             hasProperty("name", is("Núcleo de Suporte à Ação Inspetiva"))))))
         .andExpect(model().attribute(ConstantsHelper.VIEW_ATTRIBUTE_SUBAREAS, hasItem(allOf(hasProperty("id", is(3)),
                                                                                             hasProperty("area", hasProperty("id", is(1))),
                                                                                             hasProperty("abbreviation", is("NADW")),
                                                                                             hasProperty("name", is("Núcleo Antifraude e Data Warehouse"))))))
         ;
   }

   @DisplayName("Test of subareas update mapping (get) by not allowed user (projectlead2)")
   @Test
   @UnitTest
   @MockScrumifyUser(username = "developer2")
   public void updateByNotAllowedUser() throws Exception {
      int SUBAREA = 1;
      
      mvc.perform(get(ConstantsHelper.MAPPING_SUBAREAS_UPDATE + ConstantsHelper.MAPPING_SLASH + SUBAREA))
         .andExpect(status().isForbidden())
         ;
   }

   @DisplayName("Test of subareas update mapping (get) by allowed user (projectdirector)")
   @Test
   @UnitTest
   @MockScrumifyUser(username = "projectdirector")  
   public void updateByAllowedUser() throws Exception {
      int SUBAREA = 1;
      
      mvc.perform(get(ConstantsHelper.MAPPING_SUBAREAS_UPDATE + ConstantsHelper.MAPPING_SLASH + SUBAREA))
         .andExpect(status().isOk())
         .andExpect(view().name(ConstantsHelper.VIEW_SUBAREAS_UPDATE))
         .andExpect(model().attribute(ConstantsHelper.VIEW_ATTRIBUTE_SUBAREA, isA(SubAreaView.class)))
         .andExpect(model().attribute(ConstantsHelper.VIEW_ATTRIBUTE_SUBAREA, hasProperty("id", is(greaterThan(0)))))
         .andExpect(model().attribute(ConstantsHelper.VIEW_ATTRIBUTE_SUBAREA, hasProperty("area", hasProperty("id", is(greaterThan(0))))))
         .andExpect(model().attribute(ConstantsHelper.VIEW_ATTRIBUTE_SUBAREA, hasProperty("abbreviation", is("NIGC"))))
         .andExpect(model().attribute(ConstantsHelper.VIEW_ATTRIBUTE_SUBAREA, hasProperty("name", is("Núcleo de Identificação e Gestão de Contribuintes"))))
         ;
   }
}