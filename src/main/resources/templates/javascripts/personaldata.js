$(document).ready(function() {
   $('.ui.radio.checkbox').checkbox();
      
   ajaxtab(true, [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_RESOURCES_PERSONALDATA}}]], true, function(settings) {
      switch(settings.urlData.tab) {
         case "avatars":
            getavatars();
            break;
         case "personalinfo":
         default:
            getpersonalinfo();
            break;
      }
   });
   
   formValidation();
});

function getpersonalinfo() {
   var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_PUBLIC_PERSONALDATA_AJAX}}]];
   
   ajaxpost(url, $("#personaldata").serialize(), "div[data-tab='personalinfo']", false, function() {
      $('.ui.radio.checkbox').checkbox();
      
      formValidation();
   }); 
};

function getavatars() {
   var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_PUBLIC_AVATARS_PERSONALDATA_AJAX}}]];
   
   ajaxpost(url, $("#personaldata").serialize(), "div[data-tab='avatars']", false, function() {
      $('.ui.radio.checkbox').checkbox();
      
      formValidation();
   });
};
  
function getgender() {
   var gender = $("input[type=radio][name='gender']:checked").val();
     
   if (gender == null)
      return 1;
   else
      return gender;
};

function formValidation() {
   $('form').form({
      on : 'blur',
      inline : true,
      fields : {
         abbreviation_empty : {
            identifier : 'abbreviation',
            rules : [ {
               type : 'empty',
               prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
            } ]
         },
         abbreviation_length : {
            identifier : 'abbreviation',
            rules : [ {
               type : 'exactLength[3]',
               prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY_LENGTH}(3)}]]
            } ]
         },
         name : {
            identifier : 'name',
            rules : [ {
               type : 'empty',
               prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
            } ]
         },
         email_empty : {
            identifier : 'email',
            rules : [ {
               type : 'empty',
               prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
            } ]
         },
         email_email : {
            identifier : 'email',
            rules : [ {
               type : 'email',
               prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMAIL}}]]
            } ]
         },
         avatar : {
            identifier : 'avatar',
            rules : [ {
               type : 'checked',
               prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY_CHOICE}}]]
            } ]
         }
      },
      onSuccess : function(event) {
         event.preventDefault();
         
         var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_RESOURCES_PERSONALDATA}}]];
         ajaxpost(url, $("#personaldata").serialize(), "div[data-tab='personalinfo']", false);
      }
   });
}