CREATE VIEW v_timesheetsexport AS
 SELECT r.externalcode AS resource,
    vt.year,
    vt.month,
    vt.fortnight,
    "substring"((vt.code)::text, 0, 3) AS projectcode,
    vt.code AS taskcode,
    tow.externalcode AS tow,
    sum(vt.hours) AS hours
   FROM ((v_timesheetsapproval vt
     JOIN resources r ON ((vt.resource = r.id)))
     JOIN typesofwork tow ON ((vt.tow = tow.id)))
  GROUP BY r.externalcode, vt.year, vt.month, vt.fortnight, ("substring"((vt.code)::text, 0, 3)), vt.code, tow.externalcode
  ORDER BY r.externalcode, vt.year, vt.month, vt.fortnight, vt.code;