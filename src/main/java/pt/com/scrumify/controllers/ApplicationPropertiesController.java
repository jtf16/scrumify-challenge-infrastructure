package pt.com.scrumify.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import pt.com.scrumify.database.entities.Application;
import pt.com.scrumify.database.entities.ApplicationProperty;
import pt.com.scrumify.database.entities.Environment;
import pt.com.scrumify.database.services.ApplicationPropertyService;
import pt.com.scrumify.database.services.ApplicationService;
import pt.com.scrumify.database.services.PropertyService;
import pt.com.scrumify.entities.ApplicationPropertyView;
import pt.com.scrumify.helpers.ApplicationsHelper;
import pt.com.scrumify.helpers.ConstantsHelper;

@Controller
public class ApplicationPropertiesController {

   @Autowired
   private ApplicationsHelper applicationsHelper;
   @Autowired
   private ApplicationService appService;
   @Autowired
   private PropertyService propertiesService;
   @Autowired
   private ApplicationPropertyService applicationsPropertiesService;

   /*
    * CREATE PROPERTY IN APPLICATION
    */
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_APPLICATIONS_UPDATE + "') and @SecurityService.isMember('Application', #app)")
   @GetMapping(value = ConstantsHelper.MAPPING_APPLICATIONS_PROPERTY_CREATE + ConstantsHelper.MAPPING_PARAMETER_APPLICATION + ConstantsHelper.MAPPING_PARAMETER_ENVIRONMENT)
   public String create(Model model, @PathVariable int app, @PathVariable int environment) {
      Application application = this.appService.getOne(app);
      
      ApplicationPropertyView view = new ApplicationPropertyView();
      view.getPk().setApplication(application);
      view.getPk().setEnvironment(new Environment(environment));

      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_PROPERTY, view);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_PROPERTIES, this.propertiesService.getPropertyGroupNotIn(applicationsHelper.listOfProperties(application.getProperties(), environment), ConstantsHelper.DATABASE_TABLE_APPLICATIONS));

      return ConstantsHelper.VIEW_APPLICATIONS_PROPERTY;
   }

   /*
    * UPDATE PROPERTY IN APPLICATION
    */
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_APPLICATIONS_UPDATE + "') and @SecurityService.isMember('Application', #app)")
   @GetMapping(value = ConstantsHelper.MAPPING_APPLICATIONS_PROPERTY_UPDATE + ConstantsHelper.MAPPING_PARAMETER_APPLICATION+ ConstantsHelper.MAPPING_PARAMETER_ENVIRONMENT + ConstantsHelper.MAPPING_PARAMETER_PROPERTY )
   public String update(Model model, @PathVariable int app, @PathVariable int environment, @PathVariable int property) {
      ApplicationProperty applicationProperty = this.applicationsPropertiesService.getOne(property, app, environment);

      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_PROPERTY, new ApplicationPropertyView(applicationProperty));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_PROPERTIES, this.propertiesService.getOne(property));

      return ConstantsHelper.VIEW_APPLICATIONS_PROPERTY;
   }

   /*
    * SAVE PROPERTY IN APPLICATION
    */
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_APPLICATIONS_UPDATE + "') and @SecurityService.isMember('Application', #view.pk.application.id)")
   @PostMapping(value = ConstantsHelper.MAPPING_APPLICATIONS_PROPERTY_SAVE)
   public String save(Model model, @ModelAttribute(ConstantsHelper.VIEW_ATTRIBUTE_PROPERTY) @Valid ApplicationPropertyView view) {
      this.applicationsPropertiesService.save(view.translate());
      
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_PROPERTIES, this.applicationsPropertiesService.getByApplicationAndEnvironment(view.getPk().getApplication().getId(), view.getPk().getEnvironment().getId()));

      return ConstantsHelper.VIEW_FRAGMENT_APPLICATIONS_PROPERTIES;
   }

   /*
    * DELETE PROPERTY IN APPLICATION
    */
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_APPLICATIONS_UPDATE + "') and @SecurityService.isMember('Application', #app)")
   @GetMapping(value = ConstantsHelper.MAPPING_APPLICATIONS_PROPERTY_DELETE + ConstantsHelper.MAPPING_PARAMETER_APPLICATION + ConstantsHelper.MAPPING_PARAMETER_ENVIRONMENT + ConstantsHelper.MAPPING_PARAMETER_PROPERTY)
   public String delete(Model model, @PathVariable int app, @PathVariable int environment, @PathVariable int property) {
      this.applicationsPropertiesService.delete(applicationsPropertiesService.getOne(property, app, environment));
      
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_PROPERTIES, applicationsPropertiesService.getByApplicationAndEnvironment(app, environment));

      return ConstantsHelper.VIEW_FRAGMENT_APPLICATIONS_PROPERTIES;
   }
}