package pt.com.scrumify.validators;

import java.util.Date;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import pt.com.scrumify.annotations.NotEmpty;

public class NotEmptyValidator implements ConstraintValidator<NotEmpty, Date> {
   @Override
   public final void initialize(final NotEmpty annotation) {
      // Just to implement the interface
   }

   public final boolean isValid(final Date value, final ConstraintValidatorContext context) {
      return value != null;
   }
}