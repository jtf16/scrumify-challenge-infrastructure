package pt.com.scrumify;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.Banner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cache.annotation.EnableCaching;

import pt.com.scrumify.helpers.ApplicationPropertiesHelper;

/**
 * SpringBoot Application
 */
@EnableAutoConfiguration
@EnableCaching
@EnableConfigurationProperties
@SpringBootApplication
public class ScrumifyApplication {
   private static final Logger logger = LoggerFactory.getLogger(ScrumifyApplication.class);

   public static void main(String[] args) {
      SpringApplication app = new SpringApplication(ScrumifyApplication.class);

      app.setBannerMode(Banner.Mode.OFF);
      app.setLogStartupInfo(false);

      ApplicationPropertiesHelper helper = new ApplicationPropertiesHelper();
      helper.setContext(SpringApplication.run(ScrumifyApplication.class, args));
      helper.printStartUpLog(logger);
   }
}