package pt.com.scrumify.helpers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pt.com.scrumify.database.entities.Contract;
import pt.com.scrumify.database.entities.Timesheet;
import pt.com.scrumify.database.entities.TimesheetReview;
import pt.com.scrumify.database.entities.TimesheetStatus;
import pt.com.scrumify.database.entities.TypeOfWork;
import pt.com.scrumify.database.entities.Year;
import pt.com.scrumify.database.services.TimesheetReviewService;
import pt.com.scrumify.database.services.TimesheetService;
import pt.com.scrumify.entities.TimesheetView;

@Service
public class TimesheetReviewHelper {
   @Autowired
   private TimesheetService timesheetService;
   @Autowired
   private TimesheetReviewService timesheetReviewService;
   @Autowired
   private TimesheetApprovalHelper timesheetApprovalHelper;
   
   public void add(Integer typeOfWork, Integer timeSpent, Year year, Integer month, Integer fortnight, Contract contract, String code, String description) {
      TimesheetReview review = timesheetReviewService.getOne(ResourcesHelper.getResource(), year, month, fortnight, code, description);
      if (review == null) {
         review = new TimesheetReview();
         
         review.setResource(ResourcesHelper.getResource());
         review.setYear(year);
         review.setMonth(month);
         review.setFortnight(fortnight);
         review.setContract(contract);
         review.setCode(code);
         review.setDescription(description);
         review.setTimesheet(timesheetService.getSheetByFortnight(ResourcesHelper.getResource(), year.getId(), month, fortnight));
         review.setAnalysis(typeOfWork.equals(TypeOfWork.ANALYSIS) ? timeSpent : 0);
         review.setDesign(typeOfWork.equals(TypeOfWork.DESIGN) ? timeSpent : 0);
         review.setDevelopment(typeOfWork.equals(TypeOfWork.DEVELOPMENT) ? timeSpent : 0);
         review.setOther(typeOfWork.equals(TypeOfWork.OTHER) ? timeSpent : 0);
         review.setTest(typeOfWork.equals(TypeOfWork.TEST) ? timeSpent : 0);
      }
      else {
         review.setAnalysis(typeOfWork.equals(TypeOfWork.ANALYSIS) ? review.getAnalysis() + timeSpent : review.getAnalysis());
         review.setDesign(typeOfWork.equals(TypeOfWork.DESIGN) ? review.getDesign() + timeSpent : review.getDesign());
         review.setDevelopment(typeOfWork.equals(TypeOfWork.DEVELOPMENT) ? review.getDevelopment() + timeSpent : review.getDevelopment());
         review.setTest(typeOfWork.equals(TypeOfWork.TEST) ? review.getTest() + timeSpent : review.getTest());
         review.setOther(typeOfWork.equals(TypeOfWork.OTHER) ? review.getOther() + timeSpent : review.getOther());
      }
      
      timesheetReviewService.save(review);
   }
   
   /*
    * TODO: move this method to timesheet helper class
    */
   @Transactional
   public void approve(Timesheet timesheet, TimesheetView view) {
      timesheet.setReviewed(true);
      timesheet.setReviewDate(DatesHelper.now());
      timesheet.setReviewedBy(ResourcesHelper.getResource());
      timesheet.setComment(view.getComment() != null ? view.getComment().trim() : null);
      timesheet.setCommentedBy(timesheet.getComment() != null ? view.getCommentedBy() : null);
      timesheet.setStatus(new TimesheetStatus(TimesheetStatus.REVIEWED));
      
      timesheetApprovalHelper.transfer(timesheet, timesheet.getReviews());
      timesheetService.save(timesheet);
   }

   /*
    * TODO: move this method to timesheet helper class
    */
   @Transactional
   public void reject(Timesheet timesheet, TimesheetView view) {
      timesheet.setSubmitted(false);
      timesheet.setReviewedBy(ResourcesHelper.getResource());
      timesheet.setReviewDate(DatesHelper.now());
      timesheet.setCommentedBy(timesheet.getComment() != null ? view.getCommentedBy() : null);
      
      timesheetService.save(timesheet);
   }
   
   public void remove(Integer typeOfWork, Year year, Integer month, Integer fortnight, String code, String description, Integer previousHours) {
      TimesheetReview review = timesheetReviewService.getOne(ResourcesHelper.getResource(), year, month, fortnight, code, description);
      if (review != null) {
         review.setAnalysis(typeOfWork.equals(TypeOfWork.ANALYSIS) ? review.getAnalysis() - previousHours : review.getAnalysis());
         review.setDesign(typeOfWork.equals(TypeOfWork.DESIGN) ? review.getDesign() - previousHours : review.getDesign());
         review.setDevelopment(typeOfWork.equals(TypeOfWork.DEVELOPMENT) ? review.getDevelopment() - previousHours : review.getDevelopment());
         review.setTest(typeOfWork.equals(TypeOfWork.TEST) ? review.getTest() - previousHours : review.getTest());
         review.setOther(typeOfWork.equals(TypeOfWork.OTHER) ? review.getOther() - previousHours : review.getOther());
      }
      
      if (review.getTotal() == 0) {
         timesheetReviewService.delete(review.getId());
      }
      else {
         timesheetReviewService.save(review);
      }
   }
   
   public void set(Integer typeOfWork, Year year, Integer month, Integer fortnight, Contract contract, String code, String description, Integer hours) {
      TimesheetReview review = timesheetReviewService.getOne(ResourcesHelper.getResource(), year, month, fortnight, code, description);
      if (review == null) {
         review = new TimesheetReview();
         
         review.setResource(ResourcesHelper.getResource());
         review.setYear(year);
         review.setMonth(month);
         review.setFortnight(fortnight);
         review.setContract(contract);
         review.setCode(code);
         review.setDescription(description);
         review.setTimesheet(timesheetService.getSheetByFortnight(ResourcesHelper.getResource(), year.getId(), month, fortnight));
         review.setAnalysis(typeOfWork.equals(TypeOfWork.ANALYSIS) ? hours : 0);
         review.setDesign(typeOfWork.equals(TypeOfWork.DESIGN) ? hours : 0);
         review.setDevelopment(typeOfWork.equals(TypeOfWork.DEVELOPMENT) ? hours : 0);
         review.setOther(typeOfWork.equals(TypeOfWork.OTHER) ? hours : 0);
         review.setTest(typeOfWork.equals(TypeOfWork.TEST) ? hours : 0);
      }
      else {
         review.setAnalysis(typeOfWork.equals(TypeOfWork.ANALYSIS) ? hours : review.getAnalysis());
         review.setDesign(typeOfWork.equals(TypeOfWork.DESIGN) ? hours : review.getDesign());
         review.setDevelopment(typeOfWork.equals(TypeOfWork.DEVELOPMENT) ? hours : review.getDevelopment());
         review.setTest(typeOfWork.equals(TypeOfWork.TEST) ? hours : review.getTest());
         review.setOther(typeOfWork.equals(TypeOfWork.OTHER) ? hours : review.getOther());
      }
      
      if (review.getTotal() == 0) {
         timesheetReviewService.delete(review.getId());
      }
      else {
         timesheetReviewService.save(review);
      }
   }
}