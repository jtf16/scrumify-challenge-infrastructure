package pt.com.scrumify.helpers;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.TypeOfNote;
import pt.com.scrumify.database.entities.WorkItemNote;
import pt.com.scrumify.database.entities.WorkItem;
import pt.com.scrumify.database.services.ResourceService;
import pt.com.scrumify.database.services.TypeOfWorkItemNoteService;
import pt.com.scrumify.database.services.WorkItemNoteService;
import pt.com.scrumify.database.services.WorkItemService;
import pt.com.scrumify.entities.WorkItemNoteView;
import pt.com.scrumify.services.EmailService;

@Service
public class WorkItemNotesHelper {
   @Autowired
   private EmailService emailService;
   @Autowired
   private ResourceService resourcesService;   
   @Autowired
   private TypeOfWorkItemNoteService typesOfWorkItemNoteService;
   @Autowired
   private WorkItemService workItemsService;
   @Autowired
   private WorkItemNoteService workItemNoteService;
   
   @Transactional
   public WorkItemNote addNote(WorkItemNoteView view, HttpServletRequest request, HttpServletResponse response) {
      TypeOfNote typeOfNote = typesOfWorkItemNoteService.getById(view.getTypeOfNote().getId());
      view.setTypeOfNote(typeOfNote);
      
      WorkItemNote note = workItemNoteService.save(view.translate());
      WorkItem workitem = workItemsService.getOne(note.getWorkItem().getId());
      List<Resource> members = resourcesService.getByTeamExceptCurrentResource(note.getWorkItem().getTeam(), ResourcesHelper.getResource());
      
      emailService.sendWorkItemsNotificationNote(typeOfNote, workitem, note, members, request, response);
      
      return note;
   }
}