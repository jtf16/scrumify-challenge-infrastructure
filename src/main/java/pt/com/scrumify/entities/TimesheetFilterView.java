package pt.com.scrumify.entities;

import lombok.Getter;
import lombok.Setter;

public class TimesheetFilterView {
   
   @Getter
   @Setter
   public Integer year;
   
   @Getter
   @Setter
   public Integer month;
   
   @Getter
   @Setter
   public Integer fortnight;
   
   @Getter
   @Setter
   public Integer resource;
   
   public TimesheetFilterView() {
      super();
   }
   
   public TimesheetFilterView(Integer month, Integer year) {
      super();
      
      this.year = year;
      this.month = month;
   }  
   
   public TimesheetFilterView(Integer month, Integer year, Integer resource) {
      super();
      
      this.year = year;
      this.month = month;
      this.resource = resource;
   }      
}