package pt.com.scrumify.entities;

import lombok.Getter;
import lombok.Setter;
import pt.com.scrumify.database.entities.WorkItemStatus;
import pt.com.scrumify.database.entities.Workflow;
import pt.com.scrumify.database.entities.WorkflowTransition;

public class WorkflowTransitionView {
   
   @Getter
   @Setter
   private Integer id = 0;
   
   @Getter
   @Setter
   private Workflow workflow;
   
   @Getter
   @Setter
   private WorkItemStatus status;
   
   @Getter
   @Setter
   private WorkItemStatus nextStatus;
   
   @Getter
   @Setter
   private Integer order;
   
   public WorkflowTransitionView() {
      super();
   }
   
   public WorkflowTransitionView(WorkflowTransition transition) {
      super();
      
      this.setId(transition.getId());
      this.setWorkflow(transition.getWorkflow());
      this.setStatus(transition.getStatus());
      this.setNextStatus(transition.getNextStatus());
      this.setOrder(transition.getOrder());
   }
   
   public WorkflowTransition translate() {
      WorkflowTransition transition = new WorkflowTransition();
      
      transition.setId(id);
      transition.setWorkflow(workflow);
      transition.setStatus(status);
      transition.setNextStatus(nextStatus);
      transition.setOrder(order);
      
      return transition;
   }
}