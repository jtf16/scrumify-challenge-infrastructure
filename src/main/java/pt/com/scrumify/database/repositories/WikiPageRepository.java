package pt.com.scrumify.database.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import pt.com.scrumify.database.entities.WikiPage;

public interface WikiPageRepository extends JpaRepository<WikiPage, Integer> {
   
}