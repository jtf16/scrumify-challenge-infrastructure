package pt.com.scrumify.database.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import pt.com.scrumify.database.entities.Cookie;
import pt.com.scrumify.database.entities.Resource;

public interface CookieRepository extends JpaRepository<Cookie, Integer> {
   @Query(nativeQuery = false, 
          value = "SELECT c " + 
                  "FROM Cookie c " + 
                  "WHERE c.resource = :resource " + 
                  "AND c.name = :name")
   Cookie getOne(@Param("name") String name, @Param("resource") Resource resource);
}