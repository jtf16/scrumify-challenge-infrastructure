package pt.com.scrumify.database.repositories;

import java.util.List;

import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import pt.com.scrumify.database.entities.ProjectPhase;

public interface ProjectPhaseRepository extends JpaRepository<ProjectPhase, Integer> {
   @Query(nativeQuery = false,
          value = "SELECT pp " +
                  "FROM ProjectPhase pp " +
                  "ORDER BY pp.technicalAssistance, pp.sortOrder")
  List<ProjectPhase> getAll();
   
   @Cacheable(value = "projectphases", key = "#technicalAssistance")
   @Query(nativeQuery = false,
          value = "SELECT pp " +
                  "FROM ProjectPhase pp " +
                  "WHERE pp.technicalAssistance = :technicalAssistance " +
                  "ORDER BY pp.sortOrder")
   List<ProjectPhase> findByTechnicalAssistance(@Param("technicalAssistance") boolean technicalAssistance);
}