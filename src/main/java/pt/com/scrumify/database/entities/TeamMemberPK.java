package pt.com.scrumify.database.entities;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Embeddable
@NoArgsConstructor
public class TeamMemberPK implements Serializable {
   private static final long serialVersionUID = 7754762841406874946L;

   @Getter
   @Setter
   @ManyToOne(cascade = CascadeType.ALL)
   private Team team;

   @Getter
   @Setter
   @ManyToOne(cascade = CascadeType.ALL)
   private Resource resource;

   public TeamMemberPK(Team team, Resource resource) {
      super();
      
      this.team = team;
      this.resource = resource;
   }
}