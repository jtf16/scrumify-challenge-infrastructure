package pt.com.scrumify.database.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pt.com.scrumify.helpers.ConstantsHelper;
import pt.com.scrumify.helpers.DatesHelper;
import pt.com.scrumify.helpers.ResourcesHelper;

@Entity
@NoArgsConstructor
@Table(name = ConstantsHelper.DATABASE_TABLE_APPLICATIONS)
public class Application implements Serializable {
   private static final long serialVersionUID = 6904399980203715368L;

   @Id
   @Getter
   @Setter
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   @Column(name = "id", nullable = false)
   private Integer id = 0;

   @Getter
   @Setter
   @Column(name = "name", length = 80, nullable = false)
   private String name;

   @Getter
   @Setter
   @Column(name = "description", length = 150, nullable = true)
   private String description;

   @Getter
   @Setter
   @Column(name = "taxonomy", length = 50, nullable = false)
   private String taxonomy;

   @Getter
   @Setter
   @Column(name = "fqdn", length = 150, nullable = false)
   private String fqdn;
   
   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "type", nullable = false)
   private TypeOfApplication type;
   
   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "status", nullable = false)
   private ApplicationStatus status;
   
   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "subarea", nullable = false)
   private SubArea subArea;
   
   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "responsible", nullable = false)
   private Resource responsible;

   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "functionalcontact", nullable = false)
   private Resource functionalContact;

   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "technicalcontact", nullable = false)
   private Resource technicalContact;
   
   @Getter
   @Setter
   @Column(name = "active", nullable = false)
   private Boolean active = true;
   
   @Getter
   @Setter
   @OneToMany(fetch = FetchType.LAZY, mappedBy = "application")
   private List<WorkItem> workItems = new ArrayList<>(0);
   
   @Getter
   @Setter
   @OneToMany(fetch = FetchType.LAZY, mappedBy = "application", cascade = CascadeType.ALL)
   private List<ApplicationCredential> credentials = new ArrayList<>(0);
   
   @Getter
   @Setter
   @OneToMany(fetch = FetchType.LAZY, mappedBy = "application", cascade = CascadeType.ALL)
   private List<ApplicationDataSource> dataSources = new ArrayList<>(0);
   
   @Getter
   @Setter
   @OneToMany(fetch = FetchType.LAZY, mappedBy = "application", cascade = CascadeType.ALL)
   private List<ApplicationLink> links = new ArrayList<>(0);
   
   @Getter
   @Setter
   @OneToMany(fetch = FetchType.LAZY, mappedBy = "pk.application", cascade = CascadeType.ALL)
   private List<ApplicationProperty> properties = new ArrayList<>(0);

   @Getter
   @Setter
   @Column(name = "created", nullable = false)
   private Date created;

   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "createdby", nullable = false)
   private Resource createdBy;

   @Getter
   @Setter
   @Column(name = "lastupdate", nullable = false)
   private Date lastUpdate;

   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "lastupdateby", nullable = false)
   private Resource lastUpdateBy;

   public Application(int id) {
      super();

      this.id = id;
   }
   
   @PrePersist
   public void onInsert() {
      this.created = DatesHelper.now();
      this.createdBy = ResourcesHelper.getResource();
      this.lastUpdate = DatesHelper.now();
      this.lastUpdateBy = ResourcesHelper.getResource();
   }

   @PreUpdate
   public void onUpdate() {
      this.lastUpdate = DatesHelper.now();
      this.lastUpdateBy = ResourcesHelper.getResource();
   }
}