package pt.com.scrumify.database.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Embeddable
@NoArgsConstructor
public class TimesheetApprovalMonthlyPK implements Serializable {
   private static final long serialVersionUID = -3599940924307812581L;

   @Getter
   @Setter
   @Column(name = "year", nullable = false)
   private Integer year;

   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "contract", nullable = false)
   private Contract contract;
}