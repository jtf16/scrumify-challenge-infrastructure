package pt.com.scrumify.database.entities;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pt.com.scrumify.helpers.ConstantsHelper;
import pt.com.scrumify.helpers.DatesHelper;
import pt.com.scrumify.helpers.ResourcesHelper;

@Entity
@NoArgsConstructor
@Table(name = ConstantsHelper.DATABASE_TABLE_RELEASES)
public class Release implements Serializable {
   private static final long serialVersionUID = 6895328916455480305L;
   
   @Id
   @Getter
   @Setter
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   @Column(name = "id", nullable = false)
   private Integer id;
   
   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "application", nullable = false)
   private Application application;

   @Getter
   @Setter
   @Column(name = "releasedate", nullable = false)
   private Date releaseDate;

   @Getter
   @Setter
   @Column(name = "version", nullable = false)
   private Integer version;

   @Getter
   @Setter
   @Column(name = "major", nullable = false)
   private Integer major;
   
   @Getter
   @Setter
   @Column(name = "minor", nullable = true)
   private Integer minor;
   
   @Getter
   @Setter
   @Column(name = "revision", nullable = true)
   private Integer revision;
   
   @Getter
   @Setter
   @Column(name = "created", nullable = true)
   private Date created;
   
   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "createdby", nullable = false)
   private Resource createdBy;
   
   @Getter
   @Setter
   @Column(name = "lastupdate", nullable = true)
   private Date lastUpdate;
   
   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "lastupdateby", nullable = true)
   private Resource lastUpdateBy;
   
   @Getter
   @Setter
   @OneToMany(fetch = FetchType.LAZY, mappedBy = "release")
   private List<WorkItem> workItems = new ArrayList<>();

   public Release(Integer id) {
      this.id = id;
   }
   
   @Override
   public String toString() {
      return this.version.toString() + "." + this.major.toString() + 
             (this.minor != null ? "." + this.minor : "") +
             (this.revision != null ? "." + this.revision : "");
   }

   @PrePersist
   public void onInsert() {
      this.created = DatesHelper.now();
      this.createdBy = ResourcesHelper.getResource();
      this.lastUpdate = DatesHelper.now();
      this.lastUpdateBy = ResourcesHelper.getResource();
   }

   @PreUpdate
   public void onUpdate() {
      this.lastUpdate = DatesHelper.now();
      this.lastUpdateBy = ResourcesHelper.getResource();
   }
}