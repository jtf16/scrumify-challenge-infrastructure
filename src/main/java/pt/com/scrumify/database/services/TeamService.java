package pt.com.scrumify.database.services;

import java.util.List;

import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.SubArea;
import pt.com.scrumify.database.entities.Team;

public interface TeamService {
   Team getOne(Integer id);
   List<Team> getAll();
   Team save(Team team);   
   Team archive(Team team);
   List<Team> getByMember(Resource resource);
   List<Team> getByMemberAndInactive(Resource resource);
   List<Team> getByMemberAndReviewer(Resource resource);
   List<Team> getByMemberAndApprover(Resource resource);
   List<Team> getByMemberAndReviewerOrApprover(Resource resource);   
   List<Team> getByMemberExceptActual(Resource resource, Team team);
   List<Team> getByMemberId(Integer idResource);
   List<Team> getByArea (Integer idArea);
   List<Team> getBySubAreasAndResource(List<SubArea> subAreas, Resource resource);
   List<Team> getBySubAreasTeamsAndResource(List<SubArea> subAreas, List<Team> teams, Resource resource);
   boolean isMemberOfContract(Resource resource, Integer contract);
   boolean isMemberOfApplication(Resource resource, Integer subArea);
   boolean isMemberOfWorkItem(Resource resource, Integer workitem);
}