package pt.com.scrumify.database.services;

import java.util.List;

import pt.com.scrumify.database.entities.Occupation;

public interface OccupationService {
   Occupation getOne(Integer id);
   List<Occupation> getAll();
   Occupation save(Occupation occupation);
}