package pt.com.scrumify.database.services;

import pt.com.scrumify.database.entities.UserStoryNote;

public interface UserStoryNoteService {
   UserStoryNote getById(Integer id);
   UserStoryNote save(UserStoryNote userstoryNote);
}