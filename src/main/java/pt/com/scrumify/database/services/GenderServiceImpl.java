package pt.com.scrumify.database.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import pt.com.scrumify.database.entities.Gender;
import pt.com.scrumify.database.repositories.GenderRepository;

@Service
public class GenderServiceImpl implements GenderService {
   @Autowired
   private GenderRepository gendersRepository;

   @Override
   @Cacheable(value = "Genders.getOne", key = "#id", unless = "#result==null")
   public Gender getOne(Integer id) {
      return this.gendersRepository.getOne(id);
   }

   @Override
   @Cacheable(value = "Genders.all", unless = "#result==null")
   public List<Gender> getAll() {
      return this.gendersRepository.findAll();
   }
}