package pt.com.scrumify.database.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pt.com.scrumify.database.entities.Menu;
import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.repositories.MenuRepository;

@Service
public class MenuServiceImpl implements MenuService {
   @Autowired
   private MenuRepository menusRepository;

   @Override
   public Menu getOne(Integer id) {
      return this.menusRepository.getOne(id);
   }

   @Override
   public List<Menu> getByResource(Resource resource) {
      return this.menusRepository.getByResource(resource);
   }

   @Override
   public Menu save(Menu menu) {
      return this.menusRepository.save(menu);
   }
}