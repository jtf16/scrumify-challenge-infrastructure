package pt.com.scrumify.database.services;

import java.util.List;

import pt.com.scrumify.database.entities.ApplicationStatus;

public interface ApplicationStatusService {
   ApplicationStatus getOne(Integer id);
   ApplicationStatus save(ApplicationStatus status);
   List<ApplicationStatus> listAll();
}