package pt.com.scrumify.database.services;

import java.util.List;

import pt.com.scrumify.database.entities.Menu;
import pt.com.scrumify.database.entities.Resource;

public interface MenuService {
   Menu getOne(Integer id);
   List<Menu> getByResource(Resource resource);
   Menu save(Menu area);
}