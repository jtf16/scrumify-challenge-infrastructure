package pt.com.scrumify.database.services;

import java.util.List;

import pt.com.scrumify.database.entities.Gender;

public interface GenderService {
   Gender getOne(Integer id);
   List<Gender> getAll();
}